// #pragma once

#include<iostream>
#include<string.h>
#include<assert.h>
using namespace std;
namespace lzy
{
class string{
        public:
        string(const char* str="")//:_str(str)//权限会放大，不能这样初始化
        {
            _size=strlen(str);
            _capacity=_size;
            _str=new char[_capacity+1];//实际开空间的时候多开一个位置给\0,但capacity还是和size一样
            strcpy(_str,str);
        }
        string (const string& s)//现代写法
        :_str(nullptr)
        ,_size(0)
        ,_capacity(0)
        {
            string tmp(s._str);//调用构造函数，tmp和s有一样大的空间和一样的值
            this->swap(tmp);////可以不用this指针调用，因为在类里面，swap默认的左边第一个参数就是this，直接调用就可以。
        }
        string& operator=(string s)
        {
            swap(s);
            return *this;
        }
        void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
        ~string()
        {
            delete[] _str;
            _str=nullptr;
            _size=_capacity=0;
        }

        typedef char* iterator;
        iterator begin()const{
            return _str;
        }
        iterator end() const{
            return _str+_size;
        }
       size_t size()const//const 修饰类成员函数实际上修饰该成员函数隐含的 this 指针，表明在该成员函数中不能对 this 指向的类中的任何成员变量进行修改
       {
            return _size;
       }
       size_t capacity()const//写俩个共有函数接收私有变量
       {
            return _capacity;
       }
       void reserve(size_t n)
       {
            if(n>_capacity)
            {
                char* tmp=new char[n+1];
                strcpy(tmp,_str);
                delete[] _str;
                _str=tmp;
                
                _capacity=n;
            }
       }
       void resize(size_t n,char ch='\0')
       {
            //分三种情况，删除数据，不扩容增加数据，扩容增加数据，后两种情况可以合起来，因为是插入数据
            if(n>_size)
            {
                reserve(n);

                for(size_t i=_size;i<n;i++)
                {
                    _str[i]=ch;            
                }
                _size=n;
                _str[_size]='\0'; //在末尾位置加上斜杠0
            }
            else
            {
                //删除
                _str[n]='\0';
                _size=n;
            }
       }
       void clear()
       {
            _size=0;
            _str[0]='\0';
       }
       //普通对象:可读可写
char& operator[](size_t pos)
{
	assert(pos < _size);
	return _str[pos];
}
//const对象:只读
char& operator[](size_t pos)const//只读的版本
{
	assert(pos < _size);
	return _str[pos];
}
void push_back(char ch)
{
    if(_size==_capacity)
    {
        int newCapacity=_capacity == 0 ? 4 : _capacity*2;
        reserve(newCapacity);
    }
    _str[_size]=ch;
    _size++;
    _str[_size]='\0';
}
void append(const char* str)
{
    size_t len=strlen(str);

    if(_size+len > _capacity)
    {
        reserve(_size+len);
    }

    strcpy(_str+_size,str);
    _size+=len;
}
string& operator+=(char ch)
{
	push_back(ch);
	return *this;
}
string& operator+=(const char* str)
{
	append(str);
	return *this;//返回对象的引用
}
string& insert(size_t pos,char ch)//支持任意位置的插入
{
    assert(pos<=_size);
    
    if(_size==_capacity)//当相等的时候，被判定为满了，需要扩容
    {
        int newCapacity=_capacity == 0 ? 4 : _capacity * 2;
        reserve(newCapacity);
    }
    size_t end=_size+1; // 指向斜杠0的位置
    while(end>pos)
    {
        _str[end]=_str[end-1];//必须写成这样的 前往后传
        --end;
    }
    _str[pos]=ch;
    _size++;//插入一个字符 size++

    return *this;
}
// 插入一个字符串到指定位置
// 参数：
//   pos - 插入位置的索引
//   str - 要插入的字符串
// 返回值：
//   返回操作后的字符串引用
string& insert(size_t pos, const char* str) {
    assert(pos <= _size);

    // 检查是否需要扩容
    size_t len=strlen(str);
    if (_size + len > _capacity) { //对于字符串来说，判断条件发生改变
        reserve(_size+len);
    }

    int end=_size;
    while (end >= (int)pos)//这样的代码是可以支持头插的。因为end是int，pos也被强转为int了。
	{
		_str[end + len] = _str[end];
		end--;
	}

    //难理解
    strncpy(_str + pos, str, len);

	_size += len;//不要忘了将_size+=，如果不+=，那么扩容就无法正常进行
    return *this;
}
string& erase(size_t pos,size_t len=npos)
{
    assert(pos<=_size);
    if(len==npos || len+pos>=_size)//全部干掉
    {
        _str[pos]='\0';
        _size=pos;
    }
    else
    {
        strcpy(_str+pos,_str+pos+len);
        _size=_size-len;
    }
    return *this;
    ////如果不搞引用返回的话，则会发生浅拷贝因为我们没写拷贝构造，临时对象离开函数会被销毁
}
size_t find(const char ch, size_t pos = 0)const
{
	assert(pos < _size);
	while (pos < _size)//一般来说不会查找空字符，所以这里就不加=
	{
		if (_str[pos] == ch)
		{
			return pos;
		}
		pos++;//找不到就往后走
	}
	return npos;//找不到返回npos
}
size_t find(const char* str, size_t pos = 0)const
{
	assert(pos < _size);
	const char* findp = strstr(_str + pos, str);//在字符串的pos位置开始找子串
	if (findp == nullptr)
		return npos;
	return findp - _str;//由于返回值是整形，所以利用findp减去初始指针即可得位置
}

        private:
        char* _str;
        size_t _size;
        size_t _capacity;
        const static size_t npos=-1;
    };

ostream& operator<<(ostream& out, const string& s)
{
	for (auto ch : s)
	{
		out << ch;
		ch++;
	}
	/*for (size_t i = 0; i < s.size(); i++)
	{
		out << s[i];
	}*/
	return out;
}

}