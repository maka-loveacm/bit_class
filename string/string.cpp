#include <iostream>
#include <vector>
#include <assert.h>
#include <algorithm>
using namespace std;

void test_vector1()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	for (size_t i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << v.max_size() << endl;

	v.pop_back();
	v.pop_back();
	v.pop_back();
	v.pop_back();
	v.pop_back();

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector2()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	cout << v.capacity() << endl;

	v.reserve(10);
	cout << v.capacity() << endl;

	// 比当前容量小时，不缩容
	v.reserve(4);
	cout << v.capacity() << endl;

	v.resize(8);
	v.resize(15, 1);
	v.resize(3);
}

void TestVectorExpand()
{
	size_t sz;
	vector<int> v;
	v.reserve(100);
	sz = v.capacity();
	cout << "making v grow:\n";
	for (int i = 0; i < 100; ++i)
	{
		v.push_back(i);
		if (sz != v.capacity())
		{
			sz = v.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}
}

void Func(const vector<int>& v)
{
	v.size();
	v[0];
	//v[0]++;
	v.at(0);
}

void test_vector3()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.size();
	v[0];
	v[0]++;
	v.at(0);

	Func(v);
}

void test_vector4()
{
	vector<int> v;
	//v.reserve(10);
	v.resize(10);
	for (size_t i = 0; i < 10; ++i)
	{
		v[i] = i;       // assert 
		//v.at(i) = i;  // 抛异常
		//v.push_back(i);
	}
}

void test_vector5()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	v.assign(10, 1);
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);

	v.assign(v1.begin(), v1.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	string str="hello world";
    vector<char> vs;
	vs.assign(str.begin(), str.end());
	for (auto e : vs)
	{
		cout << e << " ";
	}
	cout << endl;

	vs.assign(++str.begin(), --str.end());
	for (auto e : vs)
	{
		cout << e << " ";
	}
	cout << endl;
}


void test_vector6()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
    cout << "v的元素：" ;
    for (auto e : v)
	{
		cout << e << " ";
	}
    cout <<endl;
	v.insert(v.begin(), 4);
    cout << "第一次插入" << endl;
    for (auto e : v)
	{
		cout << e << " ";
	}
    cout << endl;
	v.insert(v.begin()+2, 4);
    cout << "第二次插入" << endl;
    for (auto e : v)
	{
		cout << e << " ";
	}
    cout << endl;
	// 没有find成员
	//vector<int>::iterator it = v.find(3);

    std::vector<int> myVector = {1, 2, 3, 4, 5};
    int target = 3;
    cout << "myvector的元素：" ;
    for (auto e : v)
	{
		cout << e << " ";
	}
    cout << endl;
    // 使用 std::find 在 vector 中查找元素 target
    std::vector<int>::iterator it = std::find(myVector.begin(), myVector.end(), target);
	if (it != myVector.end()) // 如果没找到的话 find返回的就是end 也就是元素的下一个位置
	{
		myVector.insert(it, 30);//这个if很重要
	}
    cout << "第三次插入" << endl;
	for (auto e : myVector)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_vector7()
{
	vector<int> v;
	v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	cout << v.size() << endl;
	cout << v.capacity() << endl;

	// 设计理念：不动空间，不去缩容   空间换时间
	v.reserve(5);
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.resize(3);
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.clear();
	cout << v.size() << endl;
	cout << v.capacity() << endl;

	// 设计理念：时间换空间，一般缩容都是异地，代价不小，一般不要轻易使用
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.shrink_to_fit();
	cout << v.size() << endl;
	cout << v.capacity() << endl;
}

int main()
{
	
    test_vector6();
	return 0;
}