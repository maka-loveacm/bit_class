#include"Sort.h"
void PrintArray(int *a,int n)
{
    for(int i=0;i<n;++i)
    {
        cout << a[i] << " " ;
    }
    cout << endl;
}
//*************************************************************************************************//
void InsertSort(int* a,int n)
{
    for( int i=0; i<n-1 ;++i )
    {
        int end=i; //多趟循环，end的初始值为i
        int tmp=a[end+1];//记录下一个值，方便赋值
        while(end >=0 )
        {
            if(a[end] > tmp)
            {
                a[end+1] = a[end];//把大的往后放 a[end]这个时候是大数
                --end;
            }
            else
            {
                break;
            }
        }
        a[end+1]=tmp;//通过比较，找到tmp的容身之处，这个时候的end已经经过了--运算跑到了前面
    }
}
//*************************************************************************************************//
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		//gap = gap / 2;
		gap = gap / 3 + 1;

		// [0,end] 插入 end+gap [0, end+gap]有序  -- 间隔为gap的数据
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}
//*************************************************************************************************//
// 最坏时间复杂度：O(N^2)
// 最好时间复杂度：O(N^2)
 void SelectSort(int* a, int n)
{
	int begin=0;
	for(;begin<n;++begin)
	{
		for(int i=begin+1;i<n;++i)
		{
			if(a[i] < a[begin])
			{
				swap(a[i],a[begin]);
			}
			else
			{
				continue;
			}
		}
	}
}
//*************************************************************************************************//
void Bubblesort(int* a,int n)
{
	for(int j=0;j < n; j++)
	{
		int exchange =0;
		for(int i=1;i<n-j;++i)
		{
			if(a[i-1] > a[i])
			{
				swap(a[i-1],a[i]);
				exchange=1;
			}
		}
		if(exchange == 0)
		{
			break;
		}
	}
}
//*************************************************************************************************//
int GetMidIndex(int* a, int left, int right)
{
	int mid = left + (right - left) / 2;
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else // a[left] >= a[mid]
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else if (a[left] < a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}
// [left, right]
int PartSort(int* a,int left,int right)
{
	int keyi = left;
	while(left < right)
	{
		while(left < right && a[right] >= a[keyi])
		{
			--right;
		}

		while(left < right && a[left] <= a[keyi])
		{
			++left;
		}
		if(left < right)
		{
			swap(a[left] , a[right]);
		}
	}
	int meeti = left;//right也一样的，因为此时左右相遇了

	swap(a[meeti] , a[keyi]);

	return meeti;
}

// [left, right]
//直接赋值 不用交换
int PartSort2(int* a, int left, int right)
{
	// 三数取中
	int mid = GetMidIndex(a, left, right);
	swap(a[left], a[mid]);

	int key = a[left];
	int hole = left;
	while (left < right)
	{
		// 右边找小，填到左边坑
		while (left < right && a[right] >= key)
		{
			--right;
		}

		a[hole] = a[right];
		hole = right;

		// 左边找大，填到右边坑
		while (left < right && a[left] <= key)
		{
			++left;
		}

		a[hole] = a[left];
		hole = left;
	}

	a[hole] = key;//我们将三数取中取好的数据放在坑中 这也就意味着我们实现了 小 -> key -> 大 这样的区间
	return hole;
}


void QuickSort(int* a,int begin,int end)
{
	//当区间大小为1时，无左右区间，直接返回
	if(begin >= end)
	{
		return ; //递归的终止条件
	}
	int keyi  = PartSort2(a,begin,end);
	//[begin, keyi-1] keyi [keyi+1, end]
	QuickSort(a,begin,keyi-1);
	QuickSort(a,keyi+1,end);
}

//*************************************************************************************************//
void _MergeSort(int* a, int left, int right, int* tmp)
{
	//当区间大小为1时，无左右区间，直接返回
	if (left >= right)
		return;

	int mid = left + (right - left) / 2;

	//递归左区间有序
	_MergeSort(a, left, mid, tmp);
	//递归右区间有序
	_MergeSort(a, mid + 1, right, tmp);
	
	//当左右区间都有序时开始归并 -- 取小的尾插
	int left1 = left, right1 = mid;  //左区间
	int left2 = mid + 1, right2 = right;  //右区间

	//在tmp数组中的相应位置尾插
	int i = left;
	while (left1 <= right1 && left2 <= right2)
	{
		if (a[left1] <= a[left2])
		{
			tmp[i++] = a[left1++];
		}
		else
		{
			tmp[i++] = a[left2++];
		}
	}

	//将较大的数组链接到tmp后面
	while (left1 <= right1)
	{
		tmp[i++] = a[left1++];
	}
	while (left2 <= right2)
	{
		tmp[i++] = a[left2++];
	}

	//将tmp中已归并的数据拷贝到原数组的相应区间上
	memcpy(a + left, tmp + left, sizeof(int) * (right - left + 1));
}

// 归并排序递归实现
void MergeSort(int* a, int n)
{
	//开辟一个额外数组用于归并
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	//归并排序
	_MergeSort(a, 0, n - 1, tmp);

	//销毁
	free(tmp);
	tmp = NULL;
}

void test()
{
	int a[] = {10,6,7,1,3,9,4,2};
    //int b[]={9,8,7,6,5,4,3,2,1,0};
	MergeSort(a, sizeof(a) / sizeof(int));
    for(int i=0;i<10;++i)
	{
		cout << a[i] << " ";
	}cout << endl;
	// MergeSort(b, sizeof(b) / sizeof(int));
    // for(int i=0;i<10;++i)
	// {
	// 	cout << b[i] << " ";
	// }
}
void test_QuickSort()
{
	int a[] = {9,1,2,5,7,4,8,6,3,5};
    int b[]={9,8,7,6,5,4,3,2,1,0};
	QuickSort(a, 0, sizeof(a) / sizeof(int)-1);
    for(int i=0;i<10;++i)
	{
		cout << a[i] << " ";
	}cout << endl;
	QuickSort(b, 0 ,sizeof(b) / sizeof(int)-1);
    for(int i=0;i<10;++i)
	{
		cout << b[i] << " ";
	}
}
int main()
{
    test();
    return 0;
}
