#include "SList.h"


void test()
{
    SLTNode* plist = NULL;
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);

	SListPrint(plist);

	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);

	SListPopFront(&plist);
	SListPrint(plist);

	SListDestory(&plist);
}
int main()
{
	//TestSList1();
	//TestSList3();
    test();

	return 0;
}


//void swapi(int* px, int* py)
//{
//	int tmp = *px;
//	*px = *py;
//	*py = tmp;
//}
//
//void swapptr(int** ppx, int** ppy)
//{
//	int* tmp = *ppx;
//	*ppx = *ppy;
//	*ppy = tmp;
//}
//
//int main()
//{
//	int x = 0, y = 1;
//	swapi(&x, &y);
//
//	int* p1 = &x, *p2 = &y;
//	swapptr(&p1, &p2);
//
//	TestSList1();
//
//	return 0;
//}