//一个经典的期末考试题目
#include<iostream>
#include<map>
#include<string>
#include<sstream>
using namespace std;
int main()
{
    string s="1 2 3 4 5 6 7 8 9 1 2 1 2 3 4 5 6 4 2 3 4";
    //注意，这里一定是得有空格的,为了和之后的string stream对应
    map<int,int>m;
    stringstream is(s);//把字符串构造了is字符串流类对象
    while(is.eof()!=true) //如果到了文件尾部 则自动返回true 如果没到 则返回false
    {
        int num;
        //num << is;这样当然是不可以的因为num是int类型的 这样反着写是肯定不可以的！！int类型当流的左值 荒谬
        // 写 = 出 >> ; 读 = 入 << 
        is >> num ;//is的值流向num 此时num有1 因为后面有空格 无法接收更多的值
        m[num]++;//小心 不是m[num++];!!!!!!!
        //经典的统计数字的方法 如果没有就插入 如果有就将第二个值++ 而且还可以返回第二个值的大小 
        //V& operator(const key& k)
    }
    cout << m[2] << endl;
    //(*((this->insert(make_pair(k,mapped_type()))).first)).second 返回的是数字2的统计次数
    return 0;
}