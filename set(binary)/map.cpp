#include<iostream>
#include<set>
#include<map>
using namespace std;
void test_map()
{
    map<int, int> m;
    int arr[] = { 1,1,1,2,3,4,5,6 };
    for (auto& e : arr)//引用记得加上
    {
        m.insert(make_pair(e, e));
        //m.insert(pair<int,int>(e,e));
    }

    map<int, int>m1(m.begin(), m.end());//迭代器区间构造
    for(const auto& e : m1)
    {
        cout << e.first<<":"<<e.second << " "; 
    }cout << endl;

    map<int, int> m2(m1);//拷贝构造
    for (const auto& e : m2)
    {
        cout << e.first << ":" << e.second << " ";
    }cout << endl;
}
void test_map1()
{
    map<string, string> dict;
    dict.insert(pair<string, string>("排序", "sort"));
    dict.insert(pair<string, string>("左边", "left"));
    dict.insert(pair<string, string>("右边","right"));
    //make_pair()
    dict.insert(make_pair("字符串", "string"));

    //输出查看结果
    map<string, string>::iterator it = dict.begin();
    while (it != dict.end())
    {
        cout << it->first<<":"<<it->second << endl;//访问元素这么访问，一定要注意了
        ++it;
    }cout << endl;
}
void test_map_total()
{
//统计水果出现的次数
    string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果",
    "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
    map<string, int> countMap;
    for (auto& e : arr)
    {
    map<string, int>:: iterator it = countMap.find(e);
    if (it == countMap.end())//如果没找到，就插入
    {
        countMap.insert(make_pair(e, 1));
    }
    else
    {
        it->second++;//如果找到了，就给这个水果次数+1
    }
}
    map<string, int>::iterator it=countMap.begin();
    while(it!=countMap.end())
    {
        cout << it->first << ":" << it->second << endl;
        ++it;
    }cout << endl;
}
void test_map_total2()
{//统计水果出现的次数
    string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果","苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
    map<string, int> countMap;
    //我们的期末考试是用了文件流的eof来做循环
    
    for(size_t i=0; i < sizeof(arr)/sizeof(string); ++i)
    {
        countMap[arr[i]]++;//如果找不到就插入，找到就给第二个值++
    }
    
    //输出结果
    map<string, int>::iterator it=countMap.begin();
    while(it!=countMap.end())
    {
        cout << (*it).first << " " << (*it).second << endl;
        it++;
    }
}
void test_insert_change_find()
{
    map<string, string> dict;
    dict.insert(make_pair("上", "up"));
    dict.insert(make_pair("下", "down"));
    dict.insert(make_pair("左", "left"));
    dict.insert(make_pair("右", "right"));
    //插入（前提是key不在）
    dict["迭代器"];
    //修改
    dict["迭代器"]= "iterator";
    //查找（前提是得key在，key要是不在可就成了插入了）
    cout << dict["上"] << endl; cout << endl;cout << endl;

    cout << "输出map中的值" << endl;
    map<string, string>::iterator it=dict.begin();
    while (it != dict.end())
    {
        cout << it->first<<":"<<it->second << endl;
        ++it;//老忘了写
    }
    cout << endl;
}
int main()
{
    test_map_total2();
}