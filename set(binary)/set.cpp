#include<iostream>
#include<set>
using namespace std;
void test_multiset()
{
    int arr[] = {1,2,3,1,1,6,8};
    multiset<int> s(arr, arr + sizeof(arr) / sizeof(int));
    for (auto& e : s)
    {
        cout << e << " ";
    } cout << endl;
}
void multiset_test2()
{
	// 用数组array中的元素构造multiset
	int array[] = {1,2,3,3,3,5,1,2,3,4,3};
	multiset<int> s(array, array + sizeof(array) / sizeof(array[0]));

	//如果查找的key在multiset中有多个，则返回中序遍历中遇到的第一个节点的迭代器
	multiset<int>::iterator pos = s.find(3);
	while (pos != s.end()) {
		cout << *pos << " ";
		++pos;
	} cout << endl;

	// 查找+删除
	//输出key为3的节点的个数
	cout << s.count(3) << endl;//节点3个数

	pos = s.find(3);
	if (pos != s.end())
    {
        s.erase(pos);
    }

	cout << s.count(3) << endl;//节点3个数
}

void test_set3()
{
    set<int> s;
    s.insert(1);
    s.insert(2);
    s.insert(3);
    set<int>::iterator it = s.begin();
    while (it != s.end())
    {  
        cout << *it << " ";
        ++it;
    }cout << endl;//1 2 3
    auto pos = s.find(3);//谁用后面这个函数我笑话谁：auto pos = find(s.begin(), s.end(), 3);
    if (pos != s.end())
    {
        s.erase(pos);
    }
    
    for (auto e : s) cout << e << " ";

    cout << endl;//1 2
}
int main()
{
    multiset_test2();
    return 0;
}