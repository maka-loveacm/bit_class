#include<list>
#include<iostream>
#include<algorithm>
#include<vector>
#include<time.h>
using namespace std;
void test1()
{
    list<int> l1;                         // 构造空的l1
    list<int> l2(4, 100);                 // l2中放4个值为100的元素
    list<int> l3(l2.begin(), l2.end());  // 用l2的[begin(), end()）左闭右开的区间构造l3
    list<int> l4(l3);                    // 用l3拷贝构造l4

    // 以数组为迭代器区间构造l5
    int array[] = { 16,2,77,29 };
    list<int> l5(array, array + sizeof(array) / sizeof(int));

    // 列表格式初始化C++11
    list<int> l6{ 1,2,3,4,5 };

    // 用迭代器方式打印l5中的元素
    list<int>::iterator it = l5.begin();
    while (it != l5.end())
    {
        cout << *it << " ";
        ++it;
    }       
    cout << endl;

    // C++11范围for的方式遍历
    for (auto& e : l5)
        cout << e << " ";

    cout << endl;
}
void test_sort()
{
     //vector sort 和 list sort 性能对比 -- release 版本下
	srand((size_t)time(0));
	const int N = 1000000;  //100万个数据

	vector<int> v;
	v.reserve(N);
	list<int> lt;
	for (int i = 0; i < N; ++i)
	{
		auto e = rand();
		v.push_back(e);
		lt.push_back(e);
	}

	//vector sort
	int begin1 = clock();
	sort(v.begin(), v.end());
	int end1 = clock();

	//list sort
	int begin2 = clock();
	lt.sort();
	int end2 = clock();

	printf("vector sort:%d\n", end1 - begin1);
	printf("list sort:%d\n", end2 - begin2);

}
//list sort 与 将数据转移到 vector 中进行排序后拷贝回来性能对比 -- release 版本下
void test_op2()
{
	srand(time(0));
	const int N = 1000000;  //100万个数据
	list<int> lt1;
	list<int> lt2;
	for (int i = 0; i < N; ++i)
	{
		auto e = rand();
		lt1.push_back(e);
		lt2.push_back(e);
	}

	//list sort -- lt1
	int begin1 = clock();
	lt1.sort();
	int end1 = clock();

	// 将数据拷贝到vector中排序，排完以后再拷贝回来 -- lt2
	int begin2 = clock();
	vector<int> v;
	v.reserve(N);
	for (auto e : lt2)  //拷贝
	{
		v.push_back(e);
	}
	sort(v.begin(), v.end());  //排序
	lt2.assign(v.begin(), v.end());  //拷贝
	int end2 = clock();

	printf("list1 sort:%d\n", end1 - begin1);
	printf("list2 sort:%d\n", end2 - begin2);
}
void test3()
{
    vector<int> vv={1,5,7,8,9,3,4};
    list<int> l={1,5,6,7};
    vector<int>::iterator it1=vv.begin();
    list<int>::iterator it2=l.begin();
    while(it1 < vv.end())
    {
        cout << *it1 << " ";
        it1++;
    }
    cout << endl;
    // while(it2 < l.end())
    // {
    //     cout << *it2 << " ";
    //     it2++;
    // }
    while(it2 != l.end())
    {
        cout << *it2 << " ";
        it2++;
    }
    cout << endl;
}
int main()
{
    test3();
    return 0;
}