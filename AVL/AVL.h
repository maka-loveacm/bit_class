#include <assert.h>
#include <time.h>
#include<algorithm>
#include<iostream>
using namespace std;
template<class K, class V>
struct AVLTreeNode
{
	pair<K, V> _kv;
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;

	int _bf;  // balance factor

	AVLTreeNode(const pair<K, V>& kv)
		:_kv(kv)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _bf(0)
	{}
};

template<class K, class V>
struct AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:

/*----------------------------------------------插入----------------------------------------------*/
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		// 1、更新平衡因子
		while (parent) // parent为空，也就更新到根
		{
			// 新增在右，parent->bf++;
			// 新增在左，parent->bf--;
			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}

			// 是否继续更新依据：子树的高度是否变化
			// 1、parent->_bf == 0说明之前parent->_bf是 1 或者 -1
			// 说明之前parent一边高一边低，这次插入填上矮的那边，parent所在子树高度不变，不需要继续往上更新
			// 2、parent->_bf == 1 或 -1 说明之前是parent->_bf == 0，两边一样高，现在插入一边更高了，
			// parent所在子树高度变了，继续往上更新
			// 3、parent->_bf == 2 或 -2，说明之前parent->_bf == 1 或者 -1，现在插入严重不平衡，违反规则
			// 就地处理--旋转

			// 旋转：
			// 1、让这颗子树左右高度不超过1
			// 2、旋转过程中继续保持他是搜索树
			// 3、更新调整孩子节点的平衡因子
			// 4、让这颗子树的高度跟插入前保持一致
			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 旋转
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				else
				{
					assert(false);
				}

				break;
			}
			else
			{
				assert(false);
			}
		}

		return true;
	}
/*----------------------------------------------旋转------------------------------------------------*/
    void RotateL(Node* parent)
    {
    //俩个孩子 三个双亲 好好品这句话
    //1.更新孩子节点（向下分配）
    Node* subR = parent->_right;
    Node* subRL = subR->_left;
    Node* ppNode=parent->_parent;//为了避免是局部子树

    parent->_right=subRL;
    subR->_left=parent;
    //2.更新双亲节点（往回分配）
    if(subRL!=nullptr)//往回分配的小插曲 空节点没有双亲节点
    {
        subRL->_parent=parent;
    }
    parent->_parent=subR;

    //3.判断是否为局部子树 需要在parent被修改之前 写出ppNode
    if(ppNode==nullptr)
    {
        //说明是完整的树
        _root=subR;
        _root->_parent=nullptr;
    }
    else
    {
        if(ppNode->_left==parent)//虽然parent被旋转了 但是上层的ppNode还是连着parent节点 
        {
            ppNode->_left=subR;
        }
        else
        {
            ppNode->_right=subR;
        }
        
        subR->_parent = ppNode;
    }
     //4.更新平衡因子
    parent->_bf=subR->_bf=0;//更新平衡因子
    }
    void RotateR(Node* parent)
    {
        //俩个孩子 三个双亲 好好品这句话
        //1.更新孩子节点（向下分配）
        Node* subL=parent->_left;
        Node* subLR=subL->_right;
        Node* ppNode=parent->_parent;//为了避免是局部子树
        parent->_left=subLR;
        subL->_right=parent;
        //2.更新双亲节点（往回分配）
        if(subLR!=nullptr)//往回分配的小插曲 空节点没有双亲节点
        {
            subLR->_parent=parent;
        }
        parent->_parent=subL;
        //3.判断是否为局部子树 需要在parent被修改之前 写出ppNode
        if(ppNode==nullptr)
        {
            //说明是完整的树
            _root=subL;
            _root->_parent=nullptr;
        }
        else
        {
            if(ppNode->_left==parent)//虽然parent被旋转了 但是上层的ppNode还是连着parent节点 哈哈 当年的自己提出这个问题 现在的我自己解决了
            {
                ppNode->_left=subL;
            }
            else
            {
                ppNode->_right=subL;
            }
            subL->_parent = ppNode;
        }
        //4.更新平衡因子
        parent->_bf=subL->_bf=0;//更新平衡因子
    }
    void RotateLR(Node* parent)
    {
        Node* subL=parent->_left;
        Node* subLR=subL->_right;
        int bf=subLR->_bf;//为了判读平衡因子改变的三种情况

        RotateL(parent->_left);
        RotateR(parent);

        if(bf==-1)//subLR左子树插入
        {
            subL->_bf=0;
            subLR->_bf=0;
            parent->_bf=1;
        }
        if(bf==1)//subLR右子树插入
        {
            subL->_bf=-1;
            subLR->_bf=0;
            parent->_bf=0;
        }
        if(bf==0)//本身就是新节点被插入
        {
            subL->_bf=0;
            subLR->_bf=0;
            parent->_bf=0;
        }
    }

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;
		RotateR(parent->_right);
		RotateL(parent);

		if (bf == 1)
		{
			subR->_bf = 0;
			subRL->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subR->_bf = 1;
			subRL->_bf = 0;
			parent->_bf = 0;
		}
		else if (bf == 0)
		{
			subR->_bf = 0;
			subRL->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void Inorder()
	{
		_Inorder(_root);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;

		_Inorder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_Inorder(root->_right);
	}

	int Height(Node* root)
	{
		if (root == nullptr)
			return 0;
		
		int lh = Height(root->_left);
		int rh = Height(root->_right);

		return lh > rh ? lh + 1 : rh + 1;
	}
	
	bool IsBalance()
	{
		return IsBalance(_root);
	}

	bool IsBalance(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}

		int leftHeight = Height(root->_left);
		int rightHeight = Height(root->_right);

		if (rightHeight - leftHeight != root->_bf)
		{
			cout <<root->_kv.first<<"平衡因子异常" << endl;
			return false;
		}

		return abs(rightHeight - leftHeight) < 2
			&& IsBalance(root->_left)
			&& IsBalance(root->_right);
	}

private:
	Node* _root = nullptr;
};

//void TestAVLTree()
//{
//	//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
//	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
//	AVLTree<int, int> t;
//	for (auto e : a)
//	{
//		t.Insert(make_pair(e, e));
//	}
//
//	t.Inorder();
//
//	cout << t.IsBalance() << endl;
//}

void TestAVLTree()
{
	srand(time(0));
	const size_t N = 10000;
	AVLTree<int, int> t;
	for (size_t i = 0; i < N; ++i)
	{
		size_t x = rand();
		t.Insert(make_pair(x, x));
		//cout << t.IsBalance() << endl;
	}

	//t.Inorder();

	cout << t.IsBalance() << endl;
}
