#include <iostream>
#include <vector>
#include <list>

namespace lzy {
	template<class T, class Container = std::vector<T>>
	class stack {
	public:
		//构造和析构不用写，默认生成的构造和析构对于自定义类型会调用它们的构造和析构函数

		bool empty() const {
			return _Con.empty();
		}

		size_t size() const {
			return _Con.size();
		}

		T& top() {
			return _Con.back();  //数组尾部作为栈的栈顶
		}

		const T& top() const {
			return _Con.back();
		}

		void push(const T& val) {
			_Con.push_back(val);  //在数组尾部插入数据
		}

		void pop() {
			_Con.pop_back();
		}

	private:
		Container _Con;//vector<T>_Con;
	};
}
