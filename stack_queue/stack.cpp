#include<iostream>
#include<vector>
#include<stack>
#include<list>
#include"stack.h"
using namespace std;
int main()
{

    lzy::stack<int,vector<int>> st1;
    stack<int,list<int>> st2;
    st1.push(1);
    st1.push(2);
    st1.push(3);
    st1.push(4);
    while(!st1.empty())
    {
        cout << st1.top() << " ";
        st1.pop();
    }
    cout << endl;
    // cout << st1[2] << endl;

    st2.push(1);
    st2.push(2);
    st2.push(3);
    st2.push(4);
    while(!st2.empty())
    {
        cout << st2.top() << " ";
        st2.pop();
    }
    cout << endl;
    return 0;
}